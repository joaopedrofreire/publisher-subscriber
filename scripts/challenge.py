import rospy
from std_msgs.msg import Int64

def talker():
    pub = rospy.Publisher('chatter', Int64, queue_size=10)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        for talk in range(1,11):
            pub.publish(talk)
            rate.sleep()

def callback(data):
    n = data.data
    if n%2 != 0:
        print(n)

def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber('chatter', Int64, callback)

if __name__ == '__main__':
    try:
        listener()
        talker()
    except rospy.ROSInterruptException:
        pass